# -*- coding: utf-8 -*-
from genericlogger import logger
from testmap import *
from datetime import datetime

def add_doc_tag(session,doc,tag):
  session.add(Document.TagClass(tagged=doc,text=tag))
  session.commit()

def find_doc_tag(session,doc,tag):
  pass

def del_doc_tag(session,doc,tag):
  tags = [x.text for x in doc.tags]
  
def all_contract_tags(session):
  contract_tags = Base.metadata.tables['tag_contracts']
  tagx = session.query(contract_tags).all()  
  return tagx
  
def find_contract_tag(session,tag):
  logger.debug('find_contract_tag called with tag %s'%tag)
  contract_tags = Base.metadata.tables['tag_contracts']
  tagx = session.query(distinct(contract_tags.c.text)).filter(contract_tags.c.text.ilike('%'+tag+'%')).all()
  logger.debug('Found %s'%tagx)
  return tagx
  
def find_customfield(session,term):
  logger.debug('find_customfield called with term %s'%term)
  cf = session.query(CustomField).filter(CustomField.fieldname.ilike('%'+term+'%')).distinct().all()
  logger.debug('Found %s'%cf)
  return cf
  
def init_new_contract(session,nc_dict):
  ''' Accepts a dictionary object containing all necessary information to
      add a new contract (i.e. excluding the document related information),
      this function actually performs the insert. It returns the contract
      object or raises an appropriate exception'''
  logger.debug('entered init_new_contract with dict %s'%nc_dict)
  try:
    c = Contract(title=nc_dict['title'],
      fully_executed_on=nc_dict['fully_executed_on'],
      initial_termination=nc_dict['initial_termination'],
      autorenews=nc_dict['autorenews'],
      autorenew_units=nc_dict['autorenew_units'],
      autorenew_period=nc_dict['autorenew_period'],
      termination_notice_period=nc_dict['termination_notice_period'],
      termination_notice_units=nc_dict['termination_notice_units'],
      has_recurrence_limit=nc_dict['has_recurrence_limit'],
      recurrence=nc_dict['recurrence'],
      status=nc_dict['status'],contract_value=nc_dict['contract_value'],
      contract_value_currency=nc_dict['contract_value_currency'])
  except Exception,e:
    logger.debug('Failed to create Contract: %s'%str(e))
    logger.debug('Rolling back transaction')
    session.rollback()
    raise
  session.add(c)
  for p in nc_dict['parties']:
    logger.debug('Adding party %s to contract'%p.name)
    c.parties.append(p)

  taglist = nc_dict['tags']
  for t in taglist:
    logger.debug('Adding tag %s to contract'%t)
    session.add(Contract.TagClass(tagged=c,text=t))
    
  customfields = nc_dict['customfields']
  if len(customfields.keys())>0:
    for k,v in customfields.iteritems():
      logger.debug('Adding custom field %s with value %s to dbase'%(k,v))
      cf = CustomField(fieldname=k,fieldvalue=v)
      c.customfields.append(cf)
      
  r_and_o = nc_dict['r_and_o'] #list!
  for ro in r_and_o:
    rotype = 0
    if ro[0] == 'obligation': rotype=1
    c.rando.append(RandO(type=rotype,description=ro[1]))
    logger.debug('Added %s with description %s to contract'%ro)
  logger.debug('Added Contract %s to database cache - now committing...'%c)
  try:
    session.commit()
  except Exception,e:
    logger.debug('Could not flush %s to database: %s'%(c,str(e)))
    session.rollback()
    raise
  else:
    return c

# -*- coding: utf-8 -*-
from sqlalchemy import create_engine, Table
from sqlalchemy import ForeignKey
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.orm import backref, relation,ColumnProperty
from sqlalchemy.ext.declarative import declarative_base,declared_attr
from sqlalchemy import Column, Integer, String, Sequence,DateTime,Unicode
from sqlalchemy.schema import CheckConstraint
from sqlalchemy import Text, Boolean, event,distinct
from sqlalchemy.sql.expression import and_,not_,or_,text
from datetime import datetime
from genericlogger import logger
import ConfigParser
config = ConfigParser.SafeConfigParser()
config.read('global.cfg')
#engine = create_engine('sqlite:///:memory:', echo=True)
engine = create_engine('sqlite:///db.db',echo=True)
Session = sessionmaker(bind=engine)


# constraint syntax comes from http://stackoverflow.com/questions/3366687
def check_string_length(cls, key, inst):
    prop = inst.prop
    # Only interested in simple columns, not relations
    if isinstance(prop, ColumnProperty) and len(prop.columns) == 1:
        col = prop.columns[0]
        # if we have string column with a length, install a length validator
        if isinstance(col.type, String) and col.type.length:
            max_length = col.type.length
            def set_(instance, value, oldvalue, initiator):
                if len(value)>max_length:
                    raise ValueError("Length %d exceeds allowed %d" % \
                                            (len(value), max_length))
            event.listen(inst, 'set', set_)
            
def check_unicode_length(cls, key, inst):
    prop = inst.prop
    # Only interested in simple columns, not relations
    if isinstance(prop, ColumnProperty) and len(prop.columns) == 1:
        col = prop.columns[0]
        # if we have unicode column with a length, install a length validator
        if isinstance(col.type, Unicode) and col.type.length:
            max_length = col.type.length
            def set_(instance, value, oldvalue, initiator):
                if len(value)>max_length:
                    raise ValueError("Length %d exceeds allowed %d" % \
                                            (len(value), max_length))
            event.listen(inst, 'set', set_)

Base = declarative_base()

event.listen(Base, 'attribute_instrument', check_string_length)
event.listen(Base, 'attribute_instrument', check_unicode_length)

users_alerts = Table('users_alerts', Base.metadata,
  Column('id',Integer, Sequence('users_alerts_seq'),primary_key=True),
  Column('user_id', Integer, ForeignKey('users.id')),
  Column('alert_id', Integer, ForeignKey('alerts.id'))
)

interested_in = Table('users_interested_in_contracts',Base.metadata,
  Column('id',Integer, Sequence('users_interested_in_contracts_seq'),primary_key=True),
  Column('user_id',Integer,ForeignKey('users.id')),
  Column('contract_id',Integer,ForeignKey('contracts.id')),
)

contract_parties = Table('contract_parties',Base.metadata,
  Column('id',Integer, Sequence('contract_parties_seq'),primary_key=True),
  Column('party_id',Integer,ForeignKey('partys.id')),
  Column('contract_id',Integer,ForeignKey('contracts.id')),
)

contract_custom_fields = Table('contract_custom_fields',Base.metadata,
  Column('id',Integer, Sequence('contract_custom_fields_seq'),primary_key=True),
  Column('contract_id',Integer,ForeignKey('contracts.id')),
  Column('customfield_id',Integer,ForeignKey('customfields.id')),
)


class TagMixin(object):
  @declared_attr
  def tags(cls):
    class Tag(Base):
      __tablename__ = "tag_%s" % cls.__tablename__
      id = Column(Integer, primary_key=True)
      time_created = Column(DateTime, default=datetime.utcnow, nullable=False)
      row_id = Column(Integer, ForeignKey(cls.id), index=True)
      text = Column(Unicode, nullable=False, index=True)
    cls.TagClass = Tag
    return relationship(Tag, backref='tagged')


class User(Base):
  __tablename__ = 'users'
  id = Column(Integer, Sequence('user_id_seq'), primary_key=True,index=True)
  name = Column(Unicode(70),nullable=False,index=True)
  email = Column(Unicode(70),nullable=False,index=True)
  proxymail = Column(Unicode(70),nullable=False,index=True)
  password = Column(String(64),nullable=False)
  resetpin = Column(String(64),nullable=False)
  alerts = relationship('Alert', secondary=users_alerts, backref='users')
  ctime = Column(DateTime(timezone=True), default=datetime.utcnow)
  mtime = Column(DateTime(timezone=True), default=datetime.utcnow,
          onupdate=datetime.utcnow)

  def __repr__(self):
    return "<User('%s','%s')>" % (self.name.encode('utf-8'),
          self.email.encode('utf-8'))


# from http://stackoverflow.com/questions/1889251/sqlalchemy-many-to-many-relationship-on-a-single-table

FollowRelationship = Table(
    'FollowRelationship', Base.metadata,
    Column('follower_id', Integer, ForeignKey('contracts.id')),
    Column('followee_id', Integer, ForeignKey('contracts.id'))
    )
    
class Document(Base):
  __tablename__ = 'documents'
  id = Column(String(64),primary_key=True,index=True)
  is_fully_executed = Column(Boolean,default=False)
  contract_id = Column(Integer,ForeignKey('contracts.id'))
  ctime = Column(DateTime(timezone=True), default=datetime.utcnow)
  mtime = Column(DateTime(timezone=True), default=datetime.utcnow,
          onupdate=datetime.utcnow)
  creator_id = Column(Integer,ForeignKey('users.id'))
  creator = relationship('User', backref=backref('documents', order_by=ctime))
          
  def __repr__(self):
    return '<Document(%s, %s, %s)>'%(self.id,self.contract_id,
          self.is_fully_executed)
          
class RandO(Base):
  __tablename__ = 'rando'
  id = Column(Integer, Sequence('rando_id_seq'), primary_key=True,index=True)
  type = Column(Integer(1),default=0) #0 for right,1 for obligation
  status = Column(Unicode(20),index=True,nullable=False,default=u'active')
  contract_id = Column(Integer,ForeignKey('contracts.id'))
  description = Column(Unicode(768),index=True,nullable=False)
  ctime = Column(DateTime(timezone=True), default=datetime.utcnow)
  mtime = Column(DateTime(timezone=True), default=datetime.utcnow,
          onupdate=datetime.utcnow)
  creator_id = Column(Integer,ForeignKey('users.id'))
  creator = relationship('User', backref=backref('rando', order_by=id))
  
  def __repr__(self):
    t = u'right'
    if self.type == 1: t = u'obligation'
    return '<RandO(%s, %s, %s)>'%(self.id,t,self.description)
    
class CustomField(Base):
  __tablename__ = 'customfields'
  id = Column(Integer, Sequence('rando_id_seq'), primary_key=True,index=True)
  fieldname = Column(Unicode(22),index=True)
  fieldvalue = Column(Unicode(22),index=True)
  ctime = Column(DateTime(timezone=True), default=datetime.utcnow)
  mtime = Column(DateTime(timezone=True), default=datetime.utcnow,
          onupdate=datetime.utcnow)
  creator_id = Column(Integer,ForeignKey('users.id'))
  creator = relationship('User', backref=backref('customfields', order_by=id))
  contracts = relationship('Contract',
                    secondary=contract_custom_fields,
                    backref="customfields")
                    
  def __repr__(self):
    return '<CustomField(fieldname=%s fieldvalue=%s)>'%(self.fieldname,
      self.fieldvalue)
          

class Contract(Base,TagMixin):
  __tablename__ = 'contracts'
  id = Column(Integer, Sequence('contract_id_seq'), primary_key=True,index=True)
  contract_value = Column(Integer,default=0,nullable=True)
  contract_value_currency = Column(Unicode(30),default=config.get('Mapping','default_currency'))
  parent_id = Column(Integer, ForeignKey('contracts.id'))
  status = Column(Unicode(20),default='active',index=True)
  followers = relation('Contract',secondary=FollowRelationship,
        primaryjoin=(FollowRelationship.c.followee_id==id),
        secondaryjoin=(FollowRelationship.c.follower_id==id),
        backref=backref('followees'))
  children = relationship('Contract',backref=backref('parent', remote_side=[id],
              order_by='Contract.fully_executed_on'))
  documents = relationship('Document',backref=backref('contract'))
  events = relationship('Event',backref=backref('contract'))
  rando = relationship('RandO',backref=backref('contract'))
  fully_executed_on = Column(DateTime(timezone=True),default=datetime.utcnow,
              nullable=False,index=True)
  initial_termination = Column(DateTime(timezone=True),default=None,
              nullable=True,index=True)
  autorenews = Column(Boolean,default=False)
  autorenew_units = Column(Unicode,default=None,nullable=True)
  autorenew_period = Column(Integer,default=None,nullable=True)
  termination_notice_units = Column(Unicode,default=None,nullable=True)
  termination_notice_period = Column(Integer,default=None,nullable=True)
  has_recurrence_limit = Column(Boolean,default=False)
  recurrence = Column(Integer,default=None,nullable=True)
  interested_users = relationship("User",
                    secondary=interested_in,
                    backref="interesting_contracts")  
  title = Column(Unicode(config.getint('Mapping','title_maxlength')),
      nullable=False,index=True)
  fulltext = Column(Unicode(),nullable=True,index=True)
  # does it make sense to have this to make url navigation nicer?
  nice_url_string = Column(Unicode(144),index=True)
  ctime = Column(DateTime(timezone=True), default=datetime.utcnow)
  mtime = Column(DateTime(timezone=True), default=datetime.utcnow,
          onupdate=datetime.utcnow)
  creator_id = Column(Integer,ForeignKey('users.id'))
  creator = relationship('User', backref=backref('contracts', order_by=id))

  def __repr__(self):
    if self.parent_id:
      return "<Contract('%s','%s', fully executed on %s (UTC), (belongs to %s), status is %s)>"%(self.id,
                self.title.encode('utf-8'),
                self.fully_executed_on.strftime("%Y-%m-%d"),self.parent_id,self.status)
    else:
      return "<Contract('%s','%s', fully executed on %s (UTC)),status is %s>" % (self.id,
            self.title.encode('utf-8'),
            self.fully_executed_on.strftime("%Y-%m-%d"),self.status)
            

class Party(Base):
  __tablename__ = 'partys' # I know, misspelling
  id = Column(Integer, Sequence('party_id_seq'), primary_key=True,index=True)
  name = Column(Unicode(100),nullable=False,index=True,unique=True)
  contracts = relationship('Contract',
                    secondary=contract_parties,
                    backref="parties")
                    
  def __repr__(self):
    return "<Party('%s')>"%self.name.encode('utf-8')
  

    
class Note(Base,TagMixin):
  __tablename__ = 'notes'
  id = Column(Integer, Sequence('note_id_seq'), primary_key=True,index=True)
  text = Column(Unicode(1024), nullable=False,index=True)
  ctime = Column(DateTime(timezone=True), default=datetime.utcnow)
  mtime = Column(DateTime(timezone=True), default=datetime.utcnow,
          onupdate=datetime.utcnow)
  creator_id = Column(Integer,ForeignKey('users.id'))
  creator = relationship('User', backref=backref('notes', order_by=id))
  contract_id = Column(Integer,ForeignKey('contracts.id'))
  contract = relationship('Contract', backref=backref('notes', order_by=id))

  def __repr__(self):
    return "<Note('%s','%s')>" % (self.id,self.text.encode('utf-8'))

    
class Event(Base,TagMixin):
  __tablename__ = 'events'
  id = Column(Integer, Sequence('event_id_seq'), primary_key=True,index=True)
  description = Column(Unicode(255), nullable=False,index=True)
  occurs_on = Column(DateTime(timezone=True),nullable=False)
  ctime = Column(DateTime(timezone=True), default=datetime.utcnow)
  mtime = Column(DateTime(timezone=True), default=datetime.utcnow,
          onupdate=datetime.utcnow)
  creator_id = Column(Integer,ForeignKey('users.id'))
  creator = relationship('User', backref=backref('events', order_by=id))
  contract_id = Column(Integer,ForeignKey('contracts.id'))

  def __repr__(self):
    return "<Event('%s','%s','%s')>" % (self.contract_id,self.occurs_on,
                                        self.description.encode('utf-8'))

                                        
class Alert(Base,TagMixin):
  __tablename__ = 'alerts'
  id = Column(Integer, Sequence('alert_id_seq'), primary_key=True,index=True)
  description = Column(Unicode(144), nullable=False,index=True)
  alert_on = Column(DateTime(timezone=True),nullable=False)
  ctime = Column(DateTime(timezone=True), default=datetime.utcnow)
  mtime = Column(DateTime(timezone=True), default=datetime.utcnow,
          onupdate=datetime.utcnow)
  creator_id = Column(Integer,ForeignKey('users.id'))
  creator = relationship('User', backref=backref('alerts_created', order_by=id))
  event_id = Column(Integer,ForeignKey('events.id'))
  event = relationship('Event', backref=backref('alerts', order_by=id))

  def __repr__(self):
    return "<Alert('%s','%s','%s')>" % (self.event_id,self.alert_on,
                                        self.description.encode('utf-8'))


class Comment(Base,TagMixin):
  __tablename__ = 'comments'
  id = Column(Integer, Sequence('comment_id_seq'), primary_key=True,index=True)
  text = Column(Unicode(1024), nullable=False,index=True)
  ctime = Column(DateTime(timezone=True), default=datetime.utcnow)
  mtime = Column(DateTime(timezone=True), default=datetime.utcnow,
          onupdate=datetime.utcnow)
  creator_id = Column(Integer,ForeignKey('users.id'))
  creator = relationship('User', backref=backref('comments', order_by=id))
  contract_id = Column(Integer,ForeignKey('contracts.id'))
  contract = relationship('Contract', backref=backref('comments', order_by=id))

  def __repr__(self):
    return "<Comment('%s','%s','%s')>" % (self.text.encode('utf-8'),
        self.user_id,self.ctime)


Base.metadata.create_all(engine) 

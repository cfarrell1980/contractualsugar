
        $(function () {
          
          $('#bool_autorenew').removeAttr('checked');
          $('#bool_renew_limit').removeAttr('checked');
          $('#renewal_details').hide();
          $('#renewal_period').attr("disabled", "disabled");
          $('#renewal_units').attr("disabled", "disabled");
          $('#notice_period').attr("disabled", "disabled");
          $('#notice_units').attr("disabled", "disabled");
          $('#renewal_period_limit').attr('disabled','disabled');
          $('#renewal_period_limit_div').hide();
          $('#bool_renew_limit').change(function() {
            if (this.checked) {
              $('# bool_renew_limit').attr('checked','checked')
              $('#renewal_period_limit').removeAttr('disabled');
              $('#renewal_period_limit_div').show();
            }
            else {
              $('#bool_renew_limit').removeAttr('checked')
              $('#renewal_period_limit').attr('disabled','disabled');
              $('#renewal_period_limit_div').hide();
            }
          });
          $('#bool_autorenew').change(function() {
            if(this.checked) {
              console.log('checked');
              $('#bool_autorenew').attr('checked','checked');
              $('#renewal_period').removeAttr("disabled");
              $('#renewal_units').removeAttr("disabled");
              $('#notice_period').removeAttr("disabled");
              $('#notice_units').removeAttr("disabled");
              $('#renewal_details').show();
            }
            else {
              console.log('unchecked');
              $('#bool_autorenew').removeAttr('checked');
              $('#renewal_details').hide();
              $('#renewal_period').attr("disabled", "disabled");
              $('#renewal_units').attr("disabled", "disabled");
              $('#notice_period').attr("disabled", "disabled");
              $('#notice_units').attr("disabled", "disabled");
            }
          });

          //$('#contract_title_ac').autogrow();
          $('.ac').autogrow();

          
          $('textarea[maxlength]').keyup(function(t){
            var feedback_target = $(this.parentNode).find('.charsRemaining');
		        var max = parseInt($(this).attr('maxlength'));
		        if($(this).val().length > max){
			        $(this).val($(this).val().substr(0, $(this).attr('maxlength')));
		        }
		        feedback_target.html('You have ' + (max - $(this).val().length) + ' characters remaining');
	        });
          
          $('#rangeCEFrom, #rangeCETo').daterangepicker({
            
            dateFormat: 'yy-mm-dd',
            presetRanges: [{text: 'One Year', dateStart: function(){ return Date.parse('today')  }, dateEnd: function(){ return Date.parse('Today+1 year') } },
            {text: 'Two Years', dateStart: function(){ return Date.parse('today')  }, dateEnd: function(){ return Date.parse('Today+2 years') } },
            {text: 'Three Years', dateStart: function(){ return Date.parse('today')  }, dateEnd: function(){ return Date.parse('Today+3 years') } },
            ],
            presets: {specificDate: 'Specific Date',dateRange: 'Date Range'},
            datepickerOptions: {changeYear:true}
          }
          );
          
          var cf_cache = {},
			    cflastXhr;
			    //TODO: this currently only works on the first field. Need to use .live()
		      $(".custom_field_autocomplete").autocomplete({
			      minLength: 2  ,
			      delay: 50,
			      source: function( request, response ) {
				    var term = request.term;
				    if ( term in cf_cache ) {
					    response( cf_cache[ term ] );
					    return;
				    }

				    cflastXhr = $.getJSON( "/api/customfield/autocomplete/", request, function( data, status, xhr ) {
					    cf_cache[ term ] = data;
					    if ( xhr === cflastXhr ) {
						    response( data );
					      }
				      });
			      }
		      });

          $('#party_tags').tagit({
            tagSource: function(search, showChoices) {
              var that = this;
              $.ajax({
                url: "/api/party/autocomplete/",
                data: {q: search.term},
                success: function(choices) {
                showChoices(choices);
                }
              })},
			      //itemName: 'party',
			      //fieldName: 'party_tags',
			      allowSpaces: true,
			      removeConfirmation:true,
		      });
		      $('#tag_tags').tagit({
			      tagSource: function(search, showChoices) {
              var that = this;
              $.ajax({
                url: "/api/tag/autocomplete/",
                data: {q: search.term},
                success: function(choices) {
                showChoices(choices);
                }
              })},
			      //itemName: 'tag',
			      //fieldName: 'tag_tags',
			      allowSpaces: true,
			      removeConfirmation:true,
		      });
		      
          $('#fileupload').fileupload({
             dataType: 'json',
             maxNumberOfFiles: 1,
            }
          ).bind('fileuploaddone', function (e, data) {
              console.log('bound method fileuploaddone called');
              console.log('the sha256 sum is '+data.result[0]['sha256']);
            });
        });
        
        var regex = /^(.*)(\d)+$/i;
        
        
        $('.add_event').live('click',function(e){
          var emptymsg = 'Either the date or the description was left empty';
          var eventIndex = $('.events').length;
          var hb_name = 'help-block_event_'+eventIndex;
          hb = $('#'+hb_name);
          console.log('detected click event on class add_event. eventIndex is '+eventIndex);
          var e_date_id = 'event_date_'+eventIndex;
          var e_desc_id = 'event_desc_'+eventIndex;
          var e_date = $('#'+e_date_id);
          var e_desc = $('#'+e_desc_id);
          console.log(e_date.val());
          console.log(e_desc.val());
          if((e_date.val()=='')||(e_desc.val()=='')){
            $(this).parents('.events').addClass('error');
            hb.html(emptymsg);
          } else {
            
            newidx = eventIndex+1;
            var copy =  $(this).parents(".events").clone()           
            copy.appendTo("#events_div")
              .attr("id", "events" +  eventIndex)
              .find("*").each(function() {
                var id = this.id || "";
                var match = id.match(regex) || [];
                if (match.length == 3) {	
                  this.id = match[1] + (eventIndex);
                }
              })
            var newtxt = copy.find('input[type=text]');
            newtxt.val('');
            var newtxta = copy.find('textarea');
            newtxta.val('');
            copy.find('.charsRemaining').html('');
            newtxt.attr('name','event_date_'+newidx);
            newtxt.attr('id','event_date_'+newidx);
            newtxta.attr('name','event_desc_'+newidx);
            newtxta.attr('id','event_desc_'+newidx);
            
            newtxt.removeClass('hasDatepicker').datepicker();
            console.log(newtxt)
            newtxt.bind('click',function(e){
              // TODO: how to programmatically attach the datepicker function?  
            });
            }    
          e.preventDefault();
        });
        
        $('.delete_event').live('click',function(e){
          var eventIndex = $('.events').length;
          console.log('detected click event on class delete_event');
          if(eventIndex>1){
            console.log('More than one field available. Agree to delete');
            $(this).parents(".events").remove();
          } else {
            console.log('there is only one field left. Refusing to delete');
          }
          e.preventDefault();
        });
        
        
        var cloneIndex = $(".clonedInput").length;

        $(".add_custom_field").live("click", function(e){
          var cfa = $(this).parents('.clonedInput').find('input[type=text][name="custom_field_name_"'+cloneIndex+']');
          var cfv = $(this).parents('.clonedInput').find('input[type=text][name="custom_field_value_"'+cloneIndex+']');

          if (cfa.val() == ''){
            console.log('cfa is empty');
            $(this).parents('.clonedInput').addClass('error');
            $(this).parents('.clonedInput').find('.help-block.cfa').html('Both fields needs to be filled out');
            e.preventDefault();
          }
          else if (cfv.val() == '') {
            console.log('cfv is empty');
            $(this).parents('.clonedInput').addClass('error');
            $(this).parents('.clonedInput').find('.help-block.cfa').html('Both fields needs to be filled out');
            e.preventDefault();
          }
          else {
            console.log('both cfa and cfv appear to be filled');
            console.log('cfa='+cfa.val()+' and cfv='+cfv.val());
            $(this).parents('.clonedInput').removeClass('error');
            $(this).parents('.clonedInput').find('.help-block.cfa').html();
            $(this).parents('.clonedInput').find('.help-block.cfv').html();
            var copy =  $(this).parents(".clonedInput").clone()           
            copy.appendTo("#custom_field_div")
              .attr("id", "clonedInput" +  cloneIndex)
              .find("*").each(function() {
                var id = this.id || "";
                var match = id.match(regex) || [];
                if (match.length == 3) {
                  this.id = match[1] + (cloneIndex);
                }
              })
              .find('input[type=text]').val('');
            $(copy).find('input[type=text]').first().attr('name','custom_field_name_'+cloneIndex);
            $(copy).find('input[type=text]:eq(1)').attr('name','custom_field_value_'+cloneIndex);
            cloneIndex++;
            console.log('Current value of cloneIndex is '+cloneIndex);
            e.preventDefault();
          }
        });

      $(".delete_custom_field").live("click", function(e){
        var cfa = $(this).parents('.clonedInput').find('input[type=text][name="custom_field_name[]"]');
        var cfv = $(this).parents('.clonedInput').find('input[type=text][name="custom_field_value[]"]');
        console.log('delete_custom_field detected click()');
        if(cloneIndex > 1){
          console.log('Detected more than one custom field so agreeing to remove one');
          $(this).parents(".clonedInput").remove();
          cloneIndex--;
        } else {
          console.log('Refusing to remove custom field as there would then be none left');
          cfa.val('');
          cfv.val('');
          $(this).parents('.clonedInput').removeClass('error');
          $(this).parents('.clonedInput').find('.help-block.cfa').html("");
          e.preventDefault();
        }
        e.preventDefault();
      });
      
        // .roClonedInput == .clonedInput
        var roIndex = $(".roClonedInput").length;
        $(".add_ro").live("click", function(e){
          console.log('.add_ro click detected for roIndex '+roIndex);
          //TODO: Fix this horrible line
          container = $(this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode);
          radio_r = container.find('input[type=radio]').first()
          radio_o = container.find('input[type=radio]:eq(1)')

          var existingRadioRight = null;
          var existingRadioObligation = null;
          if(radio_r.attr('checked')=='checked') {
            console.log('setting existingRadioRight to checked');
            existingRadioRight = 'checked';
          } else {
            console.log('setting existingRadioObligation to checked');
            existingRadioObligation = 'checked';
          }
          console.log('existingRadioObligation is '+existingRadioObligation);
          console.log('existingRadioRight is '+existingRadioRight);          
          helpblock = container.find('.help-block');
          ta = container.find('textarea');
          if (ta.val()=='') {
            console.log('description is empty - refusing addition request');
            container.addClass('error');
            helpblock.html('Fill out the description before adding another');
          } else {
            console.log('description if not empty - allowing addition request');
            copy = container.clone();
            copy.appendTo("#rights_and_obligations_div")
              .attr("id", "roClonedInput" +  roIndex)
              .find("*").each(function() {
                var id = this.id || "";
                var match = id.match(regex) || [];
                if (match.length == 3) {
                  this.id = match[1] + (cloneIndex);
                }
              })
              .find('textarea').val('');
            var copyradio = copy.find('input[type=radio]').first();
            console.log('copy radio is '+ copyradio.attr('checked'));
            
            roIndex++;
            copy.removeClass('error');
            container.removeClass('error');
            container.find('.help-block').html('');
            copy.find('.help-block').html('');
            newright = copy.find('input[type=radio]').first();
            newob = copy.find('input[type=radio]:eq(1)');
            newta = copy.find('textarea').first();
            newta.attr('name','ro_desc_'+roIndex);
            newright.attr('checked','checked');
            newright.attr('name','ro_'+roIndex);
            newob.attr('name','ro_'+roIndex);
            console.log('setting value of checked for existing radio right to '+existingRadioRight);
            radio_r.attr('checked',existingRadioRight);
            console.log('setting value of checked for existing radio obligation to '+existingRadioObligation);
            radio_o.attr('checked',existingRadioObligation);
          }
          e.preventDefault();
        });
        
        
        $(".delete_ro").live("click", function(e){
          console.log('.delete_ro click detected for roIndex '+roIndex);
          container = $(this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode);
          if (roIndex > 1) {
            console.log('roIndex is greater than 1. agree to delete');
            container.remove();
            roIndex--;
          } else {
            console.log('removing this would mean there would be none left. disagree');
          }
          e.preventDefault();
        });
        
        
        $(function() {
		      $( ".datepicker" ).datepicker();
	      });
	      
	      //$(document).on('click', '.datepicker', function () {
        //  $(this).datepicker();
        //});

        
        $(function() {
        // TODO: make default help texts that we can replace if an error is rectified by the user. Currently
        // what we do is to set the help field to empty
        function showResponse(responseText, statusText, xhr, $form)  {
            if(responseText['glb_status'] == 'ok'){
              var id = responseText['values']['id'];
              //window.location.replace('/contract/view/'+id+'/');
              flash('success','Thank you!');
            }
            
              if(!(!responseText['errors']['title'])){
                $('#contract_title_div').addClass('error');
                $('#contract_title_help').html(responseText['errors']['title']);
              }
              else {
                $('#contract_title_div').removeClass('error');
                $('#contract_title_help').html('');
              }
              if(!(!responseText['errors']['effective_from']) || !(!responseText['errors']['effective_from'])){
                $('#effective_dates_div').addClass('error');
                $('#effective_from_help').html(responseText['errors']['effective_from']);
                $('#effective_to_help').html(responseText['errors']['effective_to']);
              } else {
                $('#effective_dates_div').removeClass('error');
                $('#effective_from_help').html('');
                $('#effective_to_help').html('');
              }

              if(!(!responseText['errors']['parties'])){
                $('#contract_parties_div').addClass('error');
                $('#contract_parties_help').html(responseText['errors']['contract_parties']);
              }
              else {
                $('#contract_parties_div').removeClass('error');
                $('#contract_parties_help').html(responseText['errors']['contract_parties']);
              }

        } 
          $('#add_contract').submit(function() { 
            $(this).ajaxSubmit({
              success: showResponse
            }); 
            return false; 
          });

		      var cache = {},
			    lastXhr;
		      $("#contract_title_ac").autocomplete({
			      minLength: 3,
			      delay: 50,
			      source: function( request, response ) {
				    var term = request.term;
				    if ( term in cache ) {
					    response( cache[ term ] );
					    return;
				    }

				    lastXhr = $.getJSON( "/api/title/autocomplete/", request, function( data, status, xhr ) {
					    cache[ term ] = data;
					    if ( xhr === lastXhr ) {
						    response( data );
					      }
				      });
			      }
		      });
	      });

	  var uploadLimit = 2;
	  var have_header = false;
	  
	  var sudaemon = function(e) {
	    var real_len = $('.schmarrn').length;
	    if(real_len==0){
	      $('#have_header').remove();
	      have_header = false;
	    }
	    if(real_len < uploadLimit){
	      console.log('Enabling #fileupload as there are '+real_len+' elements');
	      $('#fileupload').attr('disabled',null);
	    } else {
	      console.log('Disabling #fileupload as there are '+real_len+' elements');	    
	      $('#fileupload').attr('disabled','disabled');
	    }
	  }
	      
	  $('.delete_uploaded_doc').live('click',function(e){
	    console.log('click event detected in class .delete_uploaded_doc');
	    var sha = $(this).attr('id');
	    var div2go = '.row.'+sha;
	    $(div2go).remove();
	    sudaemon();
	    e.preventDefault();
	  });
	  
	  

	  $(function () {
      $('#fileupload').fileupload({
        dataType: 'json',
        done: function (e, data) {
            var su = $('.schmarrn').length;
            var head = "<div class='row' id='have_header'><div class='span12'>Executed</div></div>";
            if (!have_header){
              $('#already_uploaded').append(head);
              have_header = true;
              $('#have_header').tooltip({'title':'Choose which document represents the final signed contract','placement':'left'});
            }
            $.each(data.result, function (index, file) {
                var info = "<div id='success_upload_div_"+su+"' class='schmarrn row "+file.sha256+"'><input type='hidden' value='"+file.sha256+"' name='sha256_"+su+"' /><div class='span2' style='vertical-align:middle;'><input type='radio' name='is_fully_executed' value='"+file.sha256+"' checked/></div><div class='span8' style='vertical-align:middle;'>"+file.name+" ("+file.size+")</div><div class='span2'><a id='"+file.sha256+"'class='delete_uploaded_doc btn btn-mini danger' href='#'><i class='icon-trash icon-white'></i> Delete</a></div></div>";
                console.log(info);
                $('#already_uploaded').append(info);
            });
           sudaemon();
        }
      });
    });


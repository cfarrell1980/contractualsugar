#!/usr/bin/env python
# -*- coding: utf-8 -*-

import web,os,sys,json,ConfigParser,re,urllib
from hashlib import sha256
from datetime import datetime
from genericlogger import logger
from paste.gzipper import middleware as gzipper
from systemstorage import DocStore
from web.webapi import rawinput
from testmap import *

config = ConfigParser.SafeConfigParser()
config.read('global.cfg')

urls = (
  '/', 'overview',
  '/overview','overview',
  '/preferences','preferences',
  '/feedback','feedback',
  '/contract/add/?','contract_add',
  '/contract/find/?','contract_find',
  '/contract/edit/([0-9]{1,5})/?','contract_edit',
  '/contract/view/([0-9]{1,5})/?','contract_view',
  '/contract/view/all/','contract_view_all',
  '/party/view/all/','party_view_all',
  '/party/view/(.*)','party_view',
  '/api/doc/delete/([a-f0-9]{64})','doc_delete',
  '/api/utc_datetime','utc_datetime',
  '/api/utc_date','utc_date',
  '/api/utc_time','utc_time',
  '/api/doc/adddoc/','addDoc',
  '/api/tag/autocomplete/','tag_autocomplete',
  '/api/party/autocomplete/','party_autocomplete',
  '/api/party/add/','party_add',
  '/api/title/autocomplete/','title_autocomplete',
  '/api/customfield/autocomplete/','customfield_autocomplete',
)

def fmt_datetime(fmt=config.get('Global','datetime',raw=True)):
  '''Return utc datetime in default format (see global.cfg)'''
  utcnow = datetime.utcnow()
  return utcnow.strftime(fmt)
  
def fmt_time(fmt=config.get('Global','time',raw=True)):
  '''Return utc time in default format (see global.cfg)'''
  utcnow = datetime.utcnow()
  return utcnow.strftime(fmt)
  
def fmt_date(fmt=config.get('Global','date',raw=True)):
  '''Return utc date in default format (see global.cfg)'''
  utcnow = datetime.utcnow()
  return utcnow.strftime(fmt)
  
def validate_contract_period(contract_from,contract_to,fmt='%Y-%m-%d'):
  ''' Accepts strings for contract_from and contract_to and
      parses them into datetimes. If they can't be parsed to
      valid datetimes an error is raised. If both are parsed 
      to datetime, checks if contract_from is before contract_to.
      If so, return a tuple containing the two datetimes. If not,
      raise an error'''
  e = False
  errs = {'contract_from':None,'contract_to':None}
  logger.debug('entered validate_contract_period with contract_from=%s, contract_to=%s and fmt=%s'%(contract_from,
    contract_to,fmt))
  if len(contract_from)==0:
    e = True
    errs['contract_from'] = 'An effective date must be entered for the contract'
    logger.debug('contract_from is empty (%s)'%contract_from)
  else:
    try:
      dt_from = datetime.strptime(contract_from,fmt)
    except ValueError:
      e = True
      logger.debug('contract_from format not correct (%s)'%contract_from)
      errs['contract_from'] = '(%s) format does not correspond to %s for effective date'%(contract_from,fmt)
    
  if len(contract_to)==0:
    e = True
    errs['contract_to'] = 'An initial termination date must be entered for the contract'
    logger.debug('contract_to is empty (%s)'%contract_to)
  else:
    try:
      dt_to = datetime.strptime(contract_to, fmt)
    except:
      e = True
      logger.debug('contract_to format not correct (%s)'%contract_to)
      errs['contract_to'] = '(%s) format does not correspond to %s for initial termination date'%(contract_to,fmt)

  if e:
    logger.debug('detected raised error flag. Returning dict instead of tuple')
    return errs

    
  if dt_from > dt_to:
    logger.debug('contract start and end dates not chronologically valid')
    errs['contract_from'] = 'effective date (%s) cannot be chronologically later than initial termination date (%s)'%(contract_from,contract_to)
    return errs
  else:
    logger.debug('returning tuple of chronologically valid datetime objects')
    return (dt_from,dt_to)
    
def validate_parties(session,parties):
  '''Central place to validate comma separated string of parties sent by the
  frontend. Split the string into parties and then check which already exist and
  which need to be added (and add those parties). Return a list of party objects
  to the handling controller'''
  logger.debug('entered validate_parties with %s'%parties)
  if parties=='':
    logger.debug('No parties were sent')
    raise StandardError,'A contract must have at least one party'
  partylist = [x.strip() for x in parties.split(',')]
  logger.debug('the list of parties sent is %s'%partylist)
  q = session.query(Party).filter(Party.name.in_(partylist)).all()
  logger.debug('The following parties sent by user were in the database')
  logger.debug([x.name for x in q])
  diff = list(set(partylist) - set([x.name for x in q]))
  if len(diff)==0:
    logger.debug('All parties sent were already in the database')
    logger.debug('Returning list of existing party objects')
    return q
  logger.debug('The list of parties not yet in the database is %s'%diff)
  logger.debug('Adding the not yet existing parties to the database')
  session.add_all([Party(name=u'%s'%x) for x in diff])
  try:
    session.commit()
  except Exception,e:
    logger.debug('Failed to add %s to the database: %s'%(diff,str(e)))
    session.rollback()
    raise
  else:
    logger.debug('added %s to database. Returning list of Party objects'%diff)
  return session.query(Party).filter(Party.name.in_(partylist)).all()
  
def validate_tags(tags):
  '''Central place to validate comma separated string of tags sent by the
  frontend. Split the string into tags and then check which already exist and
  which need to be added (and add those tags). Return a list of tag strings
  to the handling controller. Don't return tag objects yet because we don't 
  know if the contract object has been created'''
  pass
  
def validate_docs(docs):
  '''Validate list of sha256 sums to ensure that the documents already exist'''
  logger.debug('entered validate_docs with docs=%s'%docs)
  status,tmp = True,{}
  for k,cs in docs.iteritems():
    tmpfile = os.path.join(config.get('Storage','tmpcache'),cs)
    logger.debug('Looking for file at %s'%tmpfile)
    tmp[cs] = False
    storage = DocStore(cs)
    if storage.docExists():
      logger.debug('%s exists at %s'%(cs,tmpfile))
      tmp[cs] = True
    else:
      status = False
      logger.debug('no file found for %s'%cs)
  return (status,tmp)
      
def validate_title(title):
  '''Refactored validation of title here as it may be used from different classes'''
  logger.debug('entered validate_title with title=%s'%title)
  logger.debug('title is of type %s'%type(title))
  maxlentitle = config.getint('Mapping','title_maxlength')
  logger.debug('read config Mapping:title_maxlength to be %d'%maxlentitle)
  if len(title) > maxlentitle:
    logger.debug('raising ValueError as title (%s) is %d characters long'%(title,len(title)))
    raise ValueError, 'title field cannot be longer than %d'%maxlentitle
  elif len(title)==0:
    logger.debug('raising ValueError as title (%s) is zero characters long'%(title))
    raise ValueError, 'title field must be longer than zero characters'
  else:
    return True
    
def validate_customfields(fields,values):
  pass
    
  

web.template.Template.globals['render'] = web.template.render
web.template.Template.globals['len'] = len
web.template.Template.globals['utc_datetime'] = fmt_datetime
web.template.Template.globals['utc_time'] = fmt_time
web.template.Template.globals['utc_date'] = fmt_date
web.template.Template.globals['title_maxlength'] = config.getint('Mapping',
  'title_maxlength')
web.template.Template.globals['max_doc_num'] = config.getint('Mapping',
  'max_doc_num')
web.template.Template.globals['ro_maxlength'] = config.getint('Mapping',
  'ro_maxlength')
web.template.Template.globals['default_currency'] = config.get('Mapping',
  'default_currency')
render = web.template.render('templates/', base='demolayout')
app = web.application(urls, globals())

def get_file_size(file):
  file.seek(0, 2) # Seek to the end of the file
  size = file.tell() # Get the position of EOF
  file.seek(0) # Reset the file position to the beginning
  return size

class overview:
  def GET(self):
    return render.overview()

class preferences:
  def GET(self):
    return render.preferences()

class feedback:
  def GET(self):
    return render.feedback()

class contract_add:
  def GET(self):
    ''' Responsible for displaying form for user to create document'''
    return render.contract_add()
    
  def POST(self):
    ''' Responsible for taking basic contract data and adding it to the dbase'''
    print web.input()
    session = Session()
    errors = {'effective_from':None,'effective_to':None,'title':None,
              'tags':None,'parties':None}
    values = {'effective_from':None,'effective_to':None,'title':None,'tags':None,
              'parties':None}
    have_error = False
    storage = web.input()
    customfields,docs = {},{}
    r_and_o = [] #need list - can't store by 'right' or 'obligation' as overwritten
    for k,v in storage.iteritems():
      if k.startswith('sha256_'):
        id = k.split('_')[-1]
        if v != "" and storage['sha256_%s'%id] != "":
          docs[v] = storage['sha256_%s'%id]
          values[k] = v
          values['sha256_%s'%id] = storage['sha256_%s'%id]
      elif k.startswith('custom_field_name'):
        id = k.split('_')[-1]
        if v != "" and storage['custom_field_value_%s'%id] != "":
          customfields[v] = storage['custom_field_value_%s'%id]
          values[k] = v
          values['custom_field_value_%s'%id] = storage['custom_field_value_%s'%id]
      elif (k.startswith('ro_') and ('desc' not in k)) :
        id = k.split('_')[-1]
        if v != "" and storage['ro_desc_%s'%id] != "": # if none added there will be one empty
          tmp = (v,storage['ro_desc_%s'%id])
          r_and_o.append(tmp)
          values[k] = v
          values['ro_desc_%s'%id] = storage['ro_desc_%s'%id]
    title = web.input(title=None).title
    values['title'] = title
    contract_value = web.input(contract_value=0).contract_value
    contract_value_currency = web.input(contract_value_currency=config.get('Mapping','default_currency')).contract_value_currency
    values['contract_value'] = contract_value
    values['contract_value_currency'] = contract_value_currency
    status = web.input(status=None).status
    if not status:
      status = unicode(config.get('Mapping','default_status'))
    else:
      if status.lower() in ['active','terminated','amended','unknown']:
        status = status.lower()
      else:
        status = 'fixme'
    values['status'] = 'status'
    errors['status'] = status
    try:
      validate_title(title)
    except ValueError,e:
      errors['title'] = str(e)
      have_error = True
    autorenews = web.input(bool_autorenew=False).bool_autorenew
    if autorenews == 'on': autorenews = True
    values['bool_autorenew'] = autorenews
    has_recurrence_limit = web.input(bool_renew_limit=False).bool_renew_limit
    if has_recurrence_limit == 'on': has_recurrence_limit = True
    values['bool_renew_limit'] = has_recurrence_limit
    recurrence = web.input(recurrence=None).recurrence
    values['recurrence'] = recurrence
    autorenew_units = web.input(renewal_units=None).renewal_units
    values['renewal_units'] = autorenew_units
    autorenew_period = web.input(renewal_period=None).renewal_period
    values['renewal_period'] = autorenew_period
    termination_notice_period = web.input(notice_period=None).notice_period
    values['notice_period'] = termination_notice_period
    termination_notice_units = web.input(notice_units=None).notice_units
    values['notice_units'] = termination_notice_units
    parties = web.input(parties=None).parties
    values['parties'] = parties
    tags = web.input(tags=None).tags
    values['tags'] = tags
    effective_from = web.input(effective_from=None).effective_from
    values['effective_from'] = effective_from
    effective_to = web.input(effective_to=None).effective_to
    values['effective_to'] = effective_to
    docs = validate_docs(docs)
    if not docs[0]:
      logger.debug('failed to validate docs')
      errors['docs'] = 'Failed to locate one of the documents'
      have_error = True
    
    try:
      parties = validate_parties(session,parties)
    except Exception,e:
      logger.debug('failed to validate parties: %s'%str(e))
      errors['parties'] = str(e)
      have_error = True
    
    period = validate_contract_period(effective_from,effective_to)
    if isinstance(period,dict):#err
      errors['effective_to'] = period['contract_to']
      errors['effective_from'] = period['contract_from']
      have_error = True
    else: period_tuple = period
    # TODO: decide if an else is appropriate here (kind of has to be a tuple)
    web.header('Content-Type','application/json')
    if not have_error:
      nc_dict = {'title':title,'fully_executed_on':period_tuple[0],
        'initial_termination':period_tuple[1],'autorenews':autorenews,
        'autorenew_units':autorenew_units,'autorenew_period':autorenew_period,
        'termination_notice_period':termination_notice_period,
        'termination_notice_units':termination_notice_units,
        'has_recurrence_limit':has_recurrence_limit,'recurrence':recurrence,
        'parties':parties,'tags':tags.split(','),
        'customfields':customfields,'status':status,
        'r_and_o':r_and_o,'contract_value':contract_value,
        'contract_value_currency':contract_value_currency,'docs':docs}
      from dbaseapi import init_new_contract
      try:
        c = init_new_contract(session,nc_dict)
      except Exception,e:
        logger.debug('Failed to insert contract: %s'%str(e))
        flash = 'Although your input is valid, it was not possible to add the contract to the database at this time. Sorry'
        return json.dumps({'errors':errors,'values':values,'flash':flash,
          'glb_status':'err'})
      else:
        flash = 'The contract was added to the database. Thank you!'
        values['id'] = c.id
        return json.dumps({'errors':errors,'values':values,'flash':flash,
          'glb_status':'ok'})
      finally:
        session.close()
    return json.dumps({'errors':errors,'values':values,'glb_status':'err'})

class addDoc():

  def HEAD(self):
    logger.debug('createDoc.HEAD')
    web.header('Access-Control-Allow-Origin','*')
    web.header('Access-Control-Allow-Methods','OPTIONS, HEAD, GET, POST, PUT, DELETE')
    
  def POST(self):
    logger.debug('createDoc.POST')
    web.header('Access-Control-Allow-Origin','*')
    web.header('Access-Control-Allow-Methods','OPTIONS, HEAD, GET, POST, PUT, DELETE')
    s = json.dumps(self.__handle_upload__(declared_length=int(web.ctx.env['CONTENT_LENGTH'])),
            separators=(',',':'))
    logger.debug(s)
    web.header('Content-Type','application/json')
    return s

    
  def delete(self):
    return None
    
  def __handle_upload__(self,declared_length=0):
    '''Does donkey work of storing document. Called from POST'''
    logger.debug('createDoc.__handle_upload__')
    fs = rawinput().get("files[]")
    results = []
    result = {}
    doc_preexists=False

    result['name'] = re.sub(r'^.*\\', '',fs.filename)
    result['type'] = fs.type
    allowed = config.get("Global","allowed_doc_types").split(',')
    print allowed
    if fs.type not in allowed:
      print "%s is not a recognised file format"%fs.type
    else:
      print "%s is a recognised file format"%fs.type
    size = get_file_size(fs.file)

    result['size'] = size
    mysha256 = sha256(fs.value).hexdigest()
    tmpfile = os.path.join(config.get('Storage','tmpcache'),mysha256)
    fd = open(tmpfile,'wb')
    fd.write(fs.file.read())
    fd.close()
    result['sha256'] = mysha256
    result['delete_type'] = 'DELETE'
    result['delete_url'] = '/api/doc/delete/%s'%mysha256
    result['thumbnail_url'] = 'data:image/png;base64,kHqP8FRMRc.../2011-12-14-204306.jpg'
    result['url'] = '/doc/view/%s'%mysha256
    result['doc_preexists'] = False
    storage = DocStore(mysha256)
    if storage.docExists():
      result['doc_preexists'] = True
    else:
      logger.debug('Here is where the actual storage is done')
      storage.storeDoc(tmpfile)
    results.append(result)
    return results
    
  def OPTIONS(self):
    logger.debug('createDoc.OPTIONS')
    web.header('Access-Control-Allow-Origin','*')
    web.header('Access-Control-Allow-Methods','OPTIONS, HEAD, GET, POST, PUT, DELETE')
    
  def GET(self):
    logger.debug('createDoc.GET')
    web.header("Access-Control-Allow-Origin", "*")
    web.header('Access-Control-Allow-Methods','OPTIONS, HEAD, GET, POST, PUT, DELETE')
    return ''
    
class contract_find:
  def GET(self):
    return render.contract_find()

class contract_edit:
  def GET(self,shasum):
    '''Responsible for displaying form for user to modify document'''
    ds = DocStore(shasum)
    if not ds.docExists():
      return 'You wanted to modify %s but it does not exist'%shasum
    else:
      return 'You want to modify %s and it does exist'%shasum

      
class contract_view:
  def GET(self,id):
    session = Session()
    try:
      q = session.query(Contract).filter(Contract.id==id).one()
    except Exception,e:
      logger.debug('No contract found with id %s'%id)
      session.close()
      return render.contract_not_found(id=id)
    finally:
      session.close()
    return render.contract_view(contract=q)
      
class contract_delete:
  def DELETE(self,shasum):
    ds = DocStore(shasum)
    logger.debug('doc_delete called with shasum %s'%shasum)
    if not ds.docExists():
      logger.debug('no document exists with shasum %s'%shasum)
      return json.dumps({'msg':'not so good'})
    else:
      logger.debug('deleting document with shasum %s'%shasum)
      return json.dumps({'msg':'ok'})
      
  def GET(self,shasum):
    pass
    #haha - direct calls can come over GET whereas ajax uses DELETE
      
class utc_datetime:
  def GET(self):
    '''Return current UTC datetime as text'''
    web.header('Content-Type','text/plain')
    return fmt_datetime()
    
class utc_date:
  def GET(self):
    '''Return current UTC date as text'''
    web.header('Content-Type','text/plain')
    return fmt_date()
    
class utc_time:
  def GET(self):
    '''Return current UTC time as text'''
    web.header('Content-Type','text/plain')
    return fmt_time()    

class tag_autocomplete:
  def GET(self):
    web.header('Content-Type','application/json')
    term = web.input(q='').q
    logger.debug('tag_autocomplete called with term %s'%term)
    from dbaseapi import find_contract_tag
    session = Session()
    matching = find_contract_tag(session,term)
    if len(matching)==0: return json.dumps([])
    l = []
    i=0
    for tag in matching:
      l.append({'id':str(i),'label':tag[0],'value':tag[0]})
      i+=1
    session.close()
    return json.dumps(l)
    
class party_add:
  def POST(self):
    name = web.input(name=None).name
    web.header('Content-Type','application/json')
    return json.dumps({'msg':'ok','name':name})

class party_autocomplete:
  def GET(self):
    term = web.input(q=None).q
    session = Session()
    web.header('Content-Type','application/json')
    logger.debug('party_autocomplete called with term %s'%term)
    q = session.query(Party).filter(Party.name.ilike('%'+term+'%')).all()
    l = []
    for p in q:
      tmp = {}
      tmp['id'] = str(p.id)
      tmp['label'] = p.name
      tmp['value'] = p.name
      l.append(tmp)
    session.close()
    return json.dumps(l)
    
class title_autocomplete:
  def GET(self):
    term = web.input(term=None).term
    session = Session()
    web.header('Content-Type','application/json')
    logger.debug('title_autocomplete called with term %s'%term)
    q = session.query(Contract).filter(Contract.title.ilike('%'+term+'%')).all()
    l = []
    for p in q:
      tmp = {}
      tmp['id'] = str(p.id)
      tmp['label'] = p.title
      tmp['value'] = p.title
      l.append(tmp)
    session.close()
    return json.dumps(l)

class customfield_autocomplete:
  def GET(self):
    term = web.input(term=None).term
    session = Session()
    from dbaseapi import find_customfield
    q = find_customfield(session,term)
    web.header('Content-Type','application/json')
    l = []
    for p in q:
      tmp = {}
      tmp['id'] = str(p.id)
      tmp['label'] = p.fieldname
      tmp['value'] = p.fieldname
      l.append(tmp)
    session.close()
    return json.dumps(l)


class party_view_all:
  def GET(self):
    session = Session()
    p = session.query(Party).all()
    session.close()
    return render.party_view_all(p=p)

class suggest:
  def GET(self):
    web.header('Content-Type','application/json')
    term = web.input(term='').term
    if len(term)==0:
      return json.dumps([])
    else:
      matchlist = EXPERIMENTAL_search_cache('api.opensuse.org',term)
      print matchlist
      return json.dumps(matchlist)
      
if __name__ == "__main__":
  app.run(gzipper)

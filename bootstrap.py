# -*- coding: utf-8 -*-
from genericlogger import logger
from shutil import rmtree
import ConfigParser,os
config = ConfigParser.SafeConfigParser()
config.read('global.cfg')
from testmap import *
from hashlib import sha256
from datetime import datetime
session = Session()

root = config.get('Storage', 'root')
jcache = config.get('Storage','jcache')
tcache = config.get('Storage','tmpcache')
docs = config.get('Storage','docs')

t1 = datetime.strptime("2012-02-01","%Y-%m-%d")
t2 = datetime.strptime("2012-03-23","%Y-%m-%d")
t3 = datetime.strptime("2012-04-25","%Y-%m-%d")

def setup_fs():

  if not os.path.exists(root):
    logger.debug('%s does not exist'%root)
    try:
      logger.debug('trying to create %s'%root)
      os.makedirs(root)
    except OSError,e:
      logger.error(str(e))
    else:
      logger.debug('successfully created %s'%root)
  else:
    logger.debug('%s already exists'%root)
  if not os.path.exists(jcache):
    logger.debug('%s does not exist'%jcache)
    try:
      logger.debug('trying to create %s'%jcache)
      os.makedirs(jcache)
    except OSError,e:
      logger.error(str(e))
    else:
      logger.debug('successfully created %s'%jcache)
  else:
    logger.debug('%s already exists'%jcache)
  if not os.path.exists(docs):
    logger.debug('%s does not exist'%docs)
    try:
      logger.debug('trying to create %s'%docs)
      os.makedirs(docs)
    except OSError,e:
      logger.error(str(e))
    else:
      logger.debug('successfully created %s'%docs)
  else:
    logger.debug('%s already exists'%docs)
  if not os.path.exists(tcache):
    logger.debug('%s does not exist'%tcache)
    try:
      logger.debug('trying to create %s'%tcache)
      os.makedirs(tcache)
    except OSError,e:
      logger.error(str(e))
    else:
      logger.debug('successfully created %s'%tcache)
  else:
    logger.debug('%s already exists'%tcache)

def destroy_fs():
  try:
    rmtree(root)
  except Exception,e:
    logger.error(str(e))
  else:
    logger.debug('successfully destroyed %s'%root)

def cp(s):
  x = sha256(s).hexdigest()
  logger.debug('returning hexdigest %s for %s'%(x,s))
  return x
 
setup_fs()

#destroy_fs()




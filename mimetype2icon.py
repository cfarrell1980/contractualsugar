import sys,os,ConfigParser
config = ConfigParser.SafeConfigParser()
config.read('global.cfg')
idir = config.get('Static','icons')
here = os.path.dirname(os.path.abspath(__name__))
icondir = os.path.join(here,idir)
from genericlogger import logger

class Icon:

  def __init__(self,mtype=None,size='32x32'):
    self.mtype = mtype
    self.size = size
      
  def map(self):
    if not self.mtype:
      logger.debug('mimetype None sent - returning default icon')
      return os.path.join(icondir,'default.png')
    else:
      potential = os.path.join(icondir,'%s.png'%self.mtype)
      if os.path.exists(potential):
        logger.debug('Found icon for %s'%self.mtype)
        return potential
      else:
        logger.warning('Could not find icon for %s - returning default'%self.mtype)

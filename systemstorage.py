# -*- coding: utf-8 -*-
import sys,os,shutil
from hashlib import sha256
import ConfigParser
from genericlogger import logger

config = ConfigParser.SafeConfigParser()
config.read('global.cfg')
docs = config.get('Storage', 'docs')
docsdepth = config.getint('Global','docsdepth')

class DocStore:

  def __init__(self,shasum):
    '''Sets up the DocStore object by calling __parsesha__'''
    logger.debug('DocStore.__init__ entered with shasum %s'%shasum)
    self.rawsum = shasum
    self.sumlist = [self.rawsum[i:i+docsdepth] for i in range(0, 
              len(self.rawsum), docsdepth)]
    self.sumstring =  "/".join(self.sumlist)
    self.complist = []
    self.deepestexisting = docs # assumes docs has been bootstrapped
    if not os.path.isdir(self.deepestexisting):
      logger.debug('DocStore.__init__ determined that %s does not exist'%docs)
      raise StandardError, '%s does not exist. Have you bootstrapped?'%docs
    self.__parsesha__()
    
  def __parsesha__(self):
    '''Parses a sha256 hexdigest into a filesystem location'''
    logger.debug('DocStore.__parsesha__ entered')
    leftside = self.sumstring
    logger.debug('appending the full path (%s) as first element in list'%os.path.join(docs,self.sumstring))
    self.complist.append(os.path.join(docs,self.sumstring))
    while len(leftside)>4:
      chop = leftside.rpartition('/')
      leftside = chop[0]
      self.complist.append(os.path.join(docs,leftside))
    logger.debug('now checking what the deepestexisting path is')
    for component in self.complist[1:]:
      c = os.path.join(docs,component)
      if os.path.isdir(c): # this must be the deepest existing
        self.deepestexisting = c
        break
      else:
        logger.debug('%s does NOT exist - continuing up the tree'%c)
    logger.debug('calculated the deepestexisting path to be %s'%self.deepestexisting)

  def docExists(self):
    '''Checks if the document already exists'''
    logger.debug('DocStore.docExists entered')
    if not os.path.exists(self.complist[0]):
      logger.debug('DocStore.docExists determined that %s does NOT exist'%self.complist[0])
      return False
    else:
      logger.debug('DocStore.docExists determined that %s DOES exist'%self.complist[0])
      return True

  def storeDoc(self,tmpdoc):
    '''Check if the document alread exists. If not, 
       then move the document to the appropriate filesystem location'''
    logger.debug('DocStore.storeDoc entered')
    finalpath = os.path.join(docs,self.sumstring)
    finaldir = os.path.dirname(finalpath)
    if not self.docExists():
      if not finaldir == self.deepestexisting:
        logger.debug('%s != %s'%(finaldir,self.deepestexisting))
        trydir = docs
        print self.complist
        logger.debug('Attempt to storeDoc(%s) and it does NOT exist'%self.rawsum)
        try:
          logger.debug('attempting to recursively create all needed directories')
          os.makedirs(os.path.join(docs,os.path.dirname(self.sumstring)))
        except OSError,e:
          logger.error('failed to create directory tree %s: %s'%(self.sumstring,str(e)))
          raise
        else:
          logger.debug('successfully created directory tree %s'%self.sumstring)
          
        
      else:
        logger.debug('no need to create any intermediate directories')
        logger.debug('moving the temporary document to its final home')
      try:
        shutil.move(tmpdoc,finalpath)
      except Exception,e:
        logger.error('failed to move the temporary document: %s'%str(e))
        raise
      else:
        logger.debug('successfully moved %s to %s'%(tmpdoc,finalpath))
    else:
      logger.debug('Attempt to storeDoc(%s) and it DOES exist'%self.rawsum)
      raise StandardError, '%s already exists'%self.sumstring
    
  def getDoc(self):
    '''Returns the absolute path to the requested document'''
    logger.debug('DocStore.getDoc entered')
    return self.sumstring

if __name__ == '__main__':
  testdoc = "/tmp/4345274_f201204046.pdf"
  fd = open(testdoc,'r')
  c = sha256()
  c.update(fd.read())
  fd.close()
  logger.debug('Creating DocStore object with example SHA256 hexdigest')
  d = DocStore(c.hexdigest())
  logger.debug('Checking if a document already exists with that hexdigest')
  d.storeDoc(testdoc)
    
